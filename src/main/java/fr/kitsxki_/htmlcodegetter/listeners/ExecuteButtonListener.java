package fr.kitsxki_.htmlcodegetter.listeners;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * This class is the listener who run the html code getting.
 * @see ActionListener
 * @author Kitsxki_
 */
public class ExecuteButtonListener implements ActionListener {

    /**
     * The URL text field. It's the field where the user can input the URL to request in the frame.
     */
    private final JTextField urlTextField;

    /**
     * The tag text field. It's the field where the user can input the tag to check in the html code.
     */
    private final JTextField tagTextField;

    /**
     * The file path label. This is the label where is indicated the path specified by the user.
     */
    private final JLabel fileLabel;

    /**
     * The process progress bar. This is the bar where user can find process advancement in percentage.
     */
    private final JProgressBar progressBar;

    /**
     * Class constructor.
     * @param urlTextField The url text field.
     * @param tagTextField The tag text field.
     * @param fileLabel The file path label.
     * @param progressBar The process progress bar.
     */
    public ExecuteButtonListener(JTextField urlTextField, JTextField tagTextField, JLabel fileLabel, JProgressBar progressBar) {
        this.urlTextField = urlTextField;
        this.tagTextField = tagTextField;
        this.fileLabel = fileLabel;
        this.progressBar = progressBar;
    }

    /**
     * Invoked when the user click on the "Execute" button.
     * @param event Event information.
     */
    @Override
    public void actionPerformed(ActionEvent event) {

        // Checks if the file path is the default one.
        if(this.fileLabel.getText().equals("Path to the out file...")) {
            JOptionPane.showMessageDialog(null, "Vous devez spécifier le fichier dans lequel vous souhaiter stocker le contenu de la balise.");
            return;
        }

        // The file found by the path.
        File outFile = new File(fileLabel.getText());

        // Checks if the file doesn't exist
        if(!outFile.exists()) {
            JOptionPane.showMessageDialog(null, "Le fichier spécifié n'existe pas.");
            return;
        }

        // Start a new SwingWorker to execute the task with a progress bar.
        new SwingWorker<Void, Integer>() {
            /**
             * Executed in background when worker is working.
             * @return Void
             */
            @Override
            protected Void doInBackground() {

                // The final html code (with only the asked tag at the end).
                String html;

                try {
                    // Initiate and set up the manger of the connection.
                    URLConnection connection = new URL(urlTextField.getText()).openConnection();
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
                    connection.connect();
                    // Update the advancement of the task for the progress bar.
                    this.publish(25);
                    // Initialize the variable with all the page html code. It will be turned into the tag code after.
                    html = ExecuteButtonListener.this.fetchHtmlCode(connection.getInputStream());
                    this.publish(50);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                html = ExecuteButtonListener.this.retrieveTagHtmlCode(html, tagTextField.getText());
                this.publish(75);

                try {
                    // This is the writer who will write the retrieved html code to the out file.
                    BufferedWriter writer = new BufferedWriter(new FileWriter(outFile));
                    writer.write(html);
                    writer.close();
                    this.publish(99);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                // Update the advancement to the max to signify that the task is completed.
                this.publish(100);
                return null;
            }

            /**
             * Invoked when the publish method is called.
             * @param chunks Intermediate results to process
             */
            @Override
            protected void process(List<Integer> chunks) {
                // Update the process progress bar with the updated advancement.
                progressBar.setValue(chunks.get(chunks.size() - 1));
            }

            /**
             * Invoked when the doInBackGround process was completed.
             */
            @Override
            protected void done() {
                JOptionPane.showMessageDialog(null, "Récupération du code terminée !\n");
            }
        }.execute();
    }

    /**
     * Try to fetch the html code of the requested url.
     * @param inputStream The URLConnection InputStream.
     * @see URLConnection#getInputStream()
     * @return Retrieves a String with the complete html code of the page.
     */
    private String fetchHtmlCode(InputStream inputStream) {
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            // This will be the current line who the reader is reading to each loop iteration.
            String inputLine;

            // This is the constructor that links all the lines and adds a break between them.
            StringBuilder builder = new StringBuilder();
            while((inputLine = reader.readLine()) != null)
                builder.append(inputLine).append("\n");

            return builder.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Retrieve only the html code between the specified tag.
     * @param html The html code to parse.
     * @param tag The tag where the html code to retrieve should be in.
     * @return A String with the content of the tags.
     */
    private String retrieveTagHtmlCode(String html, String tag) {
        // This is the html document created with the html code caught before.
        Document doc = Jsoup.parse(html);
        // This a list of all the elements between the specified tag only.
        Elements required = doc.select(tag);

        // This is the builder who will link all the different tags.
        StringBuilder builder = new StringBuilder();

        // Add to the StringBuilder every element in the list.
        for(Element element : required)
            builder.append(element.text());

        // Turn the builder into the final string.
        return builder.toString();
    }
}
