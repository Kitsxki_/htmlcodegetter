package fr.kitsxki_.htmlcodegetter.managers;

import fr.kitsxki_.htmlcodegetter.listeners.ExecuteButtonListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * This class manages the main frame of the application.
 * @see JFrame
 * @author Kitsxki_
 */
public class FrameManager extends JFrame {

    /**
     * Class constructor.
     * @param title Generated frame title.
     * @throws HeadlessException If GraphicsEnvironment.isHeadless() returns true.
     */
    public FrameManager(String title) throws HeadlessException {
        super(title);
    }

    /**
     * Initiate main parameters of the frame.
     * @see JFrame#setDefaultCloseOperation(int)
     * @see JFrame#setSize(int, int)
     * @see JFrame#setLayout(LayoutManager)
     */
    public void initFrame() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500, 600);
        this.setLayout(null);
    }

    /**
     * Initiate and setup components of the frame.
     * @see JTextField
     * @see Component#setBounds(int, int, int, int)
     * @see JFrame#add(Component)
     * @see JLabel
     * @see JFileChooser
     * @see JButton
     * @see JButton#addActionListener(ActionListener)
     * @see JProgressBar
     */
    public void initComponents() {
        // Create a text field for the URL.
        JLabel urlTextFieldTitle = new JLabel("Entrez l'URL :");
        urlTextFieldTitle.setBounds(65, 180, 300, 25);
        this.add(urlTextFieldTitle);
        JTextField urlTextField = new JTextField();
        urlTextField.setBounds(65, 200, 350, 25);
        this.add(urlTextField);

        // Create a text field for the tag.
        JLabel tagTextFieldTitle = new JLabel("Entrez la balise :");
        tagTextFieldTitle.setBounds(65, 230, 300, 25);
        this.add(tagTextFieldTitle);
        JTextField tagTextField = new JTextField();
        tagTextField.setBounds(65, 250, 350, 25);
        this.add(tagTextField);

        // Create a label for the out file path.
        JLabel fileLabel = new JLabel("Path to the out file...");
        fileLabel.setBounds(65, 300, 250, 25);
        this.add(fileLabel);

        // Create a button to browse the out file.
        JFileChooser fileChooser = new JFileChooser();
        JButton fileChooserButton = new JButton("Browse");
        fileChooserButton.setBounds(310, 300, 100, 25);
        fileChooserButton.addActionListener(e -> {
            int returnValue = fileChooser.showOpenDialog(fileChooserButton);

            if(returnValue == JFileChooser.APPROVE_OPTION)
                fileLabel.setText(fileChooser.getSelectedFile().getPath());
        });
        this.add(fileChooserButton);

        // Create a progress bar to track process advancement.
        JProgressBar progressBar = new JProgressBar();
        progressBar.setBounds(65, 350, 350, 25);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        this.add(progressBar);

        // Create a button to run the html getter.
        JButton button = new JButton("Execute");
        button.setBounds(125, 400, 220, 50);
        button.addActionListener(new ExecuteButtonListener(urlTextField, tagTextField, fileLabel, progressBar));
        this.add(button);
    }
}
