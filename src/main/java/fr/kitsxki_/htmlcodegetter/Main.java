package fr.kitsxki_.htmlcodegetter;

import fr.kitsxki_.htmlcodegetter.managers.FrameManager;

/**
 * This class is the main entry point of the program.
 * It contains the "main" method which is executed at startup.
 * @author Kitsxki_
 */
public class Main {

    /**
     * Entry point of the program. This method is executed at startup.
     * @param args Command line arguments passed to the program.
     */
    public static void main(String[] args) {
        // This is the main frame.
        FrameManager frame = new FrameManager("HtmlCodeGetter");
        frame.initFrame();
        frame.initComponents();
        frame.setVisible(true);
    }
}